package epam.edu.dao;

import java.util.List;

import epam.edu.model.ParticipantAssignment;

public interface ParticipantAssignmentDAO {

	public void create(ParticipantAssignment participantAssignment);

	public void remove(ParticipantAssignment participantAssignment);

	public ParticipantAssignment getParticipantAssignmentByName(String name);

	public List<ParticipantAssignment> getAllParticipantAssignments();

}
