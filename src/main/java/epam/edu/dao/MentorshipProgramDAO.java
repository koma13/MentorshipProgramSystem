package epam.edu.dao;

import java.util.List;

import epam.edu.model.MentorshipProgram;

public interface MentorshipProgramDAO {

	public void create(MentorshipProgram mentorshipProgram);

	public void remove(MentorshipProgram mentorshipProgram);

	public MentorshipProgram getMentorshipProgramByName(String name);

	public List<MentorshipProgram> getAllMentorshipPrograms();

}
