package epam.edu.dao;

import java.util.List;

import epam.edu.model.Group;

public interface GroupDAO {
	
	public void create(Group group);

	public void remove(Group group);

	public Group getGroupByName(String name);

	public List<Group> getAllGroups();
}
