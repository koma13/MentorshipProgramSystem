package epam.edu.dao;

import java.util.List;

import epam.edu.model.Person;

public interface PersonDAO {

	public void create(Person person);

	public void remove(Person person);

	public Person getPersonByName(String name);

	public List<Person> getAllPersons();
}
