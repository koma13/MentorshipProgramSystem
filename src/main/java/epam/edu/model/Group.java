package epam.edu.model;

import java.util.Date;

public class Group {
	private String domainArea;
	private String topic;
	private String lector;
	private Integer durationMin;
	private Date scheduledTime;

	public enum STATUS {
		ON_PREPARATION, SCHEDULED, PASSED
	}

	public String getDomainArea() {
		return domainArea;
	}

	public void setDomainArea(String domainArea) {
		this.domainArea = domainArea;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getLector() {
		return lector;
	}

	public void setLector(String lector) {
		this.lector = lector;
	}

	public Integer getDurationMin() {
		return durationMin;
	}

	public void setDurationMin(Integer durationMin) {
		this.durationMin = durationMin;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domainArea == null) ? 0 : domainArea.hashCode());
		result = prime * result + ((durationMin == null) ? 0 : durationMin.hashCode());
		result = prime * result + ((lector == null) ? 0 : lector.hashCode());
		result = prime * result + ((scheduledTime == null) ? 0 : scheduledTime.hashCode());
		result = prime * result + ((topic == null) ? 0 : topic.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (domainArea == null) {
			if (other.domainArea != null)
				return false;
		} else if (!domainArea.equals(other.domainArea))
			return false;
		if (durationMin == null) {
			if (other.durationMin != null)
				return false;
		} else if (!durationMin.equals(other.durationMin))
			return false;
		if (lector == null) {
			if (other.lector != null)
				return false;
		} else if (!lector.equals(other.lector))
			return false;
		if (scheduledTime == null) {
			if (other.scheduledTime != null)
				return false;
		} else if (!scheduledTime.equals(other.scheduledTime))
			return false;
		if (topic == null) {
			if (other.topic != null)
				return false;
		} else if (!topic.equals(other.topic))
			return false;
		return true;
	};

}
