package epam.edu.model;

public class ParticipantAssignment {
	private String person;

	public enum ROLE {
		MENTOR, MENTEE, CURATOR, LECTOR
	};

	public enum STATUS {
		PROPOSED, APPROVED_RM, CONFIRMED_CDP, ON_HOLD
	};
	
	public String getPerson() {
		return person;
	}
	
	public void setPerson(String person) {
		this.person = person;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((person == null) ? 0 : person.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipantAssignment other = (ParticipantAssignment) obj;
		if (person == null) {
			if (other.person != null)
				return false;
		} else if (!person.equals(other.person))
			return false;
		return true;
	}

}
